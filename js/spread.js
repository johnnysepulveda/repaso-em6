// Spread operator ...
// sirve para muchas cosas pero de las mas comunes es que permite combinar dos arreglos diferentes

let lenguajes = ['Javascript', 'PHP', 'Python'];
let frameworks = ['ReactJS', 'laravel', 'Django'];

// unimos los arreglos en uno solo

// forma vieja
// let combinacion = lenguajes.concat(frameworks);

// forma nueva
//let combinacion = [...lenguajes,...frameworks];

// tambien se pueden hacer copias del existente
//let nuevoArreglo = [...lenguajes];
//console.log(nuevoArreglo);

// ordenamos de el último hacia el primero
//let ultimo = lenguajes.reverse();

// accedemos al indice
// let [ultimo] = lenguajes.reverse();

// con spread
/*
let [ultimo] = [...lenguajes].reverse();

console.log(lenguajes);
console.log(ultimo);
*/

// otro so del spread
const suma = (a,b,c) => {
    console.log(a+b+c);
}

const numeros = [1,2,3];

suma(...numeros);


////////////////
// CONCLUSION //
////////////////

// cuando un spread esta después de un = se estan haciendo copias de arreglos
// cuando se usa solo o dentro de una función de estan concatenando dos arrays


// Definiendo el objeto tarea
import Tarea from './tarea.js';

export default class ComprasPendientes extends Tarea {
    constructor(nombre, prioridad, cantidad = 2){
        super(nombre, prioridad)
        this.cantidad = cantidad;
    }

    // podemos sobreescribir metodos del padre
    mostrar() {
        super.mostrar();
        console.log(`la cantidad 
        que desea es ${this.cantidad}`);
    }
    hola() {
        console.log('hola');
    }
}
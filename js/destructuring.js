// destructuring de objetos, o como extraer valores de un objeto

const aprendiendoJS = {
    version: {
        nueva: 'ES6',
        anterior: 'ES5'
    },
    frameworks: ['React','VueJS','AngularJS']
}

console.log(aprendiendoJS);

// versión anterior a como se accede a los valores del objeto
let version = aprendiendoJS.version.anterior;
//let framework = aprendiendoJS.frameworks[1];

// version nueva
// se debe de declarar una función con el parametro del objeto, o como se llame la funcion

let {nueva} = aprendiendoJS.version;

console.log(version);
console.log(nueva);

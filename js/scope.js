// scope

// var musica = 'Rock';
// if(musica){
//     var musica = 'Grunge';
//     console.log('Dentro del if: ', musica);
// }

// console.log('Fuera del IF', musica);

// scope con let

// let musica = 'Rock';
// if(musica){
//     let musica = 'Grunge';
//     console.log('Dentro del if: ', musica);
// }

// console.log('Fuera del IF', musica);

// uso scope con const

const musica = 'Rock';
if(musica){
    const musica = 'Grunge';
    console.log('Dentro del if: ', musica);
}

console.log('Fuera del IF', musica);
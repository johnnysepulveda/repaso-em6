// Escribir clases
import Tarea from './modulos/tarea.js';
import ComprasPendientes from './modulos/compras.js';


const tarea1 = new Tarea('Aprender Javascript', 'Alta');
const compra1 = new ComprasPendientes('Computador', 'Media', 7);

console.log(tarea1);

tarea1.mostrar();
compra1.mostrar();
compra1.hola();
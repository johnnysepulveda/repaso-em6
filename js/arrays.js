// arreglos y .map

// const carrito = ['Producto 1', 'Producto 2', 'Producto 3'];

// de esta forma me imprime en el index 
// const appContenedor = document.querySelector('#app');
// appContenedor.innerHTML = carrito;

// mejor imprimirlo de esta manera para poderle dar formato
/*
let html = '';

carrito.forEach(i => {
    html += `<li>${i}</li>`;
})
appContenedor.innerHTML = html;
*/

// imprimimos con map
// esto pegarlo en la consola del browser para ver como
// el map produce un nuevo arreglo a partir de uno
/*
carrito = ['Producto 1', 'Producto 2', 'Producto 3'];

carrito.map(producto => {
    return 'El producto es ' + producto;
} );
*/

// ahora con object.keys(objeto)
// el Object.keys(objeto) convierte un objeto en un array

const persona = {
    nombre: 'Juan',
    profesion: 'Desarrollador Web',
    edad: 500
}

const {profesion} = persona;
console.log(persona);

console.log(Object.keys(persona));
// metodos en arreglos

const personas = [
    {nombre: 'Juan', edad: 23, aprendiendo: 'Javascript'},
    {nombre: 'Alex', edad: 18, aprendiendo: 'React'},
    {nombre: 'Daniel', edad: 21, aprendiendo: 'Jumla'},
    {nombre: 'Karen', edad: 38, aprendiendo: 'Wordpress'},
    {nombre: 'Miguel', edad: 67, aprendiendo: 'Redux'},
]

console.log(personas);

// quienes son mayores de 28 años, filtramos
const edadesMayores = personas.filter(i => {
    // accedo a cada uno
    // console.log(i);
    return i.edad > 28;
});

// que esta aprendiendo alex y su edad, buscamos
const alex = personas.find(i => {
    return i.nombre === 'Alex'; 
});

//console.log(alex);
//console.log(`Alex esta aprendiendo:  ${alex.aprendiendo}`);

// reduce toma el total de las edades
// sirve para transformar la matrix en una cosa nueva
let total = personas.reduce((edadTotal, i) => {
    return edadTotal + i.edad;
}, 0);

console.log("el total es: ", total);
// promedio
console.log(`El promedio es: ${total / personas.length}`)
// funciones

// function declaration
/*
saludar('Juancho');
function saludar(nombre) {
    console.log("Bienvenido " + nombre);
}
*/

// function expression
/*
const cliente = function(nombreCliente) {
    console.log("Bienvenido Cliente: " + nombreCliente);
}
cliente('Camilo');
*/

// parametros por default de las funciones
// es asignar al parametro de entrada un valor
/*
const actividad = function(nombre ="Walter blanco", actividad="Enseño quimica"){
    console.log(`La persona ${nombre}, esta realizando la actividad ${actividad}`);
}
actividad('Juan', 'Aprender JavaScript');
actividad('Alex', 'Barrer');
actividad(); 
*/

// Arraw function

// cuando los parametros de entrada son varios
/*
let viajando = (destino,duracion) => {
    return `Esta viajando a ${destino}, tiene un recorrido aprox de: ${duracion}`;
}
*/

// cuando el parametro es uno solo y solo retorna una línea de codigo
let viajando = destino => `Esta viajando hacia: ${destino}`;

let viaje;

viaje = viajando('Francia');
viaje = viajando('U.K');
// viaje = viajando('Canada', '9 Horas');

console.log(viaje);


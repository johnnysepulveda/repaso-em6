// Objetos

// Object literal
const Persona = {
    nombre: 'Juan',
    profesion: 'Desarrollador Web',
    edad: 34
}

// object constructor
function Tarea(nombre, urgencia){
    this.nombre = nombre;
    this.urgencia = urgencia;
}

//aqui voy a agregarle al objeto Tarea la funcion mostrar
Tarea.prototype.mostrarInformacionTarea = function(){
    return `La tarea ${this.nombre} tiene una prioridad de ${this.urgencia} `;
}


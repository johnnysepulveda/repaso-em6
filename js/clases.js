// Escribir clases
import {HOLA, tareauno,tareaCompletada} from './modulos/tareas.js';

class Tarea {
    constructor(nombre, prioridad){
        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    mostrar() {
        return `${this.nombre} tiene una prioridad de ${this.prioridad}`;
    }
}

// creamos los objetos
let tarea1 = new Tarea('Aprender Javascript', 'Alta');
/*
console.log(tarea1.mostrar() );
console.log(tarea2.mostrar() );
console.log(tarea3.mostrar() );
*/

// Heredamos para crear una clase hijo
class ComprasPendientes extends Tarea {
    constructor(nombre, prioridad, cantidad){
        super(nombre, prioridad)
        this.cantidad = cantidad;
    }

    // podemos sobreescribir metodos del padre
    mostrar() {
        return `${this.nombre} tiene una prioridad de ${this.prioridad} y la cantidad 
        que desea es ${this.cantidad}`;
    }
}

// compras
let compra1 = new ComprasPendientes('Sabiduria', 'Alta', 'Mucha'); 

const TAREA1 = tareauno('hacer ejercicio', 'Alta');

console.log(compra1.mostrar());
console.log (" ");
console.log (HOLA);
console.log(tareauno('Aprender React', 'Alta'));
console.log (TAREA1);
console.log ("completo tarea:" + tareaCompletada());
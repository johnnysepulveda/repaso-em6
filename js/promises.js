// promises
// se usa cuando se hacen llamados asincronos, por ejemplo a apis

const aplicarDescuento = new Promise((resolve, reject) => {
    setTimeout( () => {
        let descuento = true;

        if(descuento){
            resolve('Descuento Aplicado!');
        } else {
            reject('Descuento no Aplicado :(');
        }
    }, 3000);
});

// cuando la promesa se cumple
aplicarDescuento.then(resultado => {
    console.log(resultado);
}).catch(error => { // en caso de que de error
    console.log(error); // me imprime el error en consola
})
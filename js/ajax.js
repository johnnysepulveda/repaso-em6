// el uso mas comun del promises en con .fetch o ajax

const descargarUsuarios = cantidad => new Promise((resolve, reject) => {
    // pasamos la cantidad a la api
    const api = `https://randomuser.me/api/?results=${cantidad}&nat=NZ`;
    // llamado ajax
    const xhr = new XMLHttpRequest();
    // abrir la conexión
    xhr.open('GET', api, true);
    // on load
    xhr.onload = () => {
        // si la peticiópn es correcta entonces se pregunta por stado 200
        if(xhr.status === 200){
            // parseamos de texto a objeto
            resolve(JSON.parse(xhr.responseText).results ); // result es cabecera de json
        } else {
            reject(Error(xhr.statusText));
        }
    }
    // opcional
    xhr.onerror = error => reject(error);
    // send
    xhr.send()
});

descargarUsuarios(20)
    .then(
        miembros => imprimirHTML(miembros),
        error => console.error(
            new Error('Hubo un error' + error)
        )
    )

// saco la información de la respuesta
imprimirHTML = usuarios => {
    let html = '';

    usuarios.forEach(usuario => {
        //console.log(usuario);
        html += ` 
            <li>
                Nombre:${usuario.name.first} ${usuario.name.last}
                País: ${usuario.nat}
                Imagen: <img src="${usuario.picture.medium}">
            </li>
        `;
    });

    const contenedorApp = document.querySelector('#app');
    contenedorApp.innerHTML = html;
}
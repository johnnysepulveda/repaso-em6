// Object literal enhacement, o como poner valores juntos 

const banda = 'Nirvana';
const genero = 'Grunge';
const canciones = ['Polly', 'Lithium', 'Territorial pisings'];

// forma anterior
// const nirvana = {
//     banda: banda,
//     genero: genero,
//     canciones: canciones
// }

// forma nueva
const nirvana = {banda, genero, canciones};

console.log(nirvana);

